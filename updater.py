
import os
import sys
import platform

root = os.path.dirname(__file__)
os.chdir(root)

os.system("git pull")

if platform.system() == "Windows":
	python = os.path.join(root, "venv", "Scripts", "python.exe")
else:
	python = os.path.join(root, "venv", "bin", "python")

if not os.path.exists("venv"):
	os.system(f"{sys.executable} -m venv venv")
os.system(f"{python} -m pip install -r requirements.txt")

os.chdir("webapp")
os.system("npm install")
os.system("npm run build")
