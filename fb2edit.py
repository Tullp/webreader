
import re
import base64

from petcmd import Commander

commander = Commander()

def read_fb2(path: str) -> bytes:
	with open(path, "rb") as f:
		return f.read()

def write_fb2(path: str, content: bytes):
	with open(path, "wb") as f:
		f.write(content)

def text_between(text: bytes, start: int, end: int, open: bytes, close: bytes) -> (int, int):
	index = text.find(open, start, end)
	if index == -1:
		return -1, -1
	start = index + len(open)
	end = text.find(close, start, end)
	return start, end

def insert(text: bytes, start: int, end: int, content: bytes) -> bytes:
	return text[:start] + content + text[end:]

def to_base64(path: str) -> bytes:
	with open(path, "rb") as file:
		return base64.b64encode(file.read())

@commander.command("annotation")
def set_annotation(path: str, annotation: str):
	"""
	Set provided annotation to the given book.

	:param path: Path to fb2 book.
	:param annotation: Annotation text to set.
	"""
	annotation_html: bytes = b"\n".join(b"<p>" + p.encode() + b"</p>" for p in annotation.splitlines())
	fb2 = read_fb2(path)
	description = text_between(fb2, 0, len(fb2), b"<description>", b"</description>")
	title_info = text_between(fb2, description[0], description[1], b"<title-info>", b"</title-info>")
	annotation_block = text_between(fb2, title_info[0], title_info[1], b"<annotation>", b"</annotation>")
	if annotation_block[0] != -1:
		fb2 = insert(fb2, annotation_block[0], annotation_block[1], annotation_html)
	elif title_info[0] != -1:
		fb2 = insert(fb2, title_info[1], title_info[1], b"<annotation>" + annotation_html + b"</annotation>")
	elif description[0] != -1:
		fb2 = insert(fb2, description[1], description[1],
			b"<title-info><annotation>" + annotation_html + b"</annotation></title-info>")
	else:
		raise Exception("Invalid fb2")
	write_fb2(path, fb2)

@commander.command("coverpage")
def set_coverpage(path: str, coverpage: str):
	"""
	Set provided coverpage to the given book.

	:param path: Path to fb2 book.
	:param coverpage: Path to coverpage image.
	"""
	new_coverpage_id = b"web-reader-coverpage"
	fb2 = read_fb2(path)
	first_binary = fb2.find(b"<binary")
	fb2 = insert(fb2, first_binary - 1, first_binary - 1,
		b"<binary id=\"" + new_coverpage_id + b"\">" + to_base64(coverpage) + b"</binary>")
	description = text_between(fb2, 0, len(fb2), b"<description>", b"</description>")
	title_info = text_between(fb2, description[0], description[1], b"<title-info>", b"</title-info>")
	coverpage_block = text_between(fb2, title_info[0], title_info[1], b"<coverpage>", b"</coverpage>")
	if coverpage_block[0] != -1:
		coverpage_id = re.search(b"href=\"#([^\"]+)\"", fb2[slice(*coverpage_block)]).group(1)
		coverpage_id_start = fb2.find(coverpage_id, coverpage_block[0], coverpage_block[1])
		fb2 = insert(fb2, coverpage_id_start, len(coverpage_id), new_coverpage_id)
	write_fb2(path, fb2)

@commander.command("add-image")
def add_image(path: str, image: str):
	"""
	Add an image to the given book.

	:param path: Path to fb2 book.
	:param image: Path to image.
	"""
	fb2 = read_fb2(path)
	first_binary_end = fb2.find(b"</binary>") + len(b"</binary>")
	fb2 = insert(fb2, first_binary_end, first_binary_end,
		b"<binary>" + to_base64(image) + b"</binary>")
	write_fb2(path, fb2)

if __name__ == "__main__":
	commander.process()
