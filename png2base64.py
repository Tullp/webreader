
import sys, base64, subprocess

def write_to_clipboard(output):
	process = subprocess.Popen("pbcopy", env={"LANG": "en_US.UTF-8"}, stdin=subprocess.PIPE)
	process.communicate(output.encode("utf-8"))

path = sys.argv[1]

with open(path, "rb") as image_file:
	write_to_clipboard(base64.b64encode(image_file.read()).decode("utf-8"))
