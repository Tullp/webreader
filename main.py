
import os
import re
import sys
import time
import json
import platform
import traceback
import urllib.request
from typing import Union

from bs4 import BeautifulSoup, Tag, NavigableString

root = os.path.dirname(os.path.realpath(__file__))
os.chdir(root)
with open("config.json", "r") as r:
	config = json.load(r)
books_storage: str = config["booksStorage"]
bot_token: str = config.get("botToken")
telegram_id: int = config.get("telegramId")

class Book:

	instances: dict[str, 'Book'] = {}

	@classmethod
	def get(cls, path: str) -> 'Book':
		if path not in cls.instances:
			cls.instances[path] = Book(path)
		return cls.instances[path]

	def __init__(self, path: str):
		self.path: str = path
		self.cycle: Cycle = Cycle.get(os.path.dirname(os.path.realpath(self.path)))
		self.name: str = os.path.basename(os.path.realpath(self.path)).rsplit(".", 1)[0]
		self.htmlpath: str = os.path.join(root, "pages", self.cycle.name, f"{self.name}.html")
		self.__fb2: bytes | None = None
		self.htmlconverter: HTMLConverter = HTMLConverter(self)
		self.__html: str | None = None

	def fb2(self) -> bytes:
		if not self.__fb2:
			with open(self.path, "rb") as f:
				self.__fb2 = f.read()
		return self.__fb2

	def html(self) -> str:
		if not self.__html:
			self.__html = self.htmlconverter.convert()
		return self.__html

	def images(self) -> str:
		return str(self.htmlconverter.collect_images())

	@staticmethod
	def valid(path: str) -> bool:
		try:
			mimetype = "text/xml"
			if platform.system() != "Windows":
				mimetype = os.popen(f"file --mime-type \"{path}\"").read().strip().split(": ", 1)[1]
			return path.endswith(".fb2") and mimetype == "text/xml"
		except Exception:
			print(traceback.format_exc())
			return False

class HTMLConverter:

	def __init__(self, book: Book):
		self.book: Book = book
		self.soup: BeautifulSoup = BeautifulSoup(self.book.fb2().split(b"?>", 1)[1], features="html.parser")
		self.__coverpage: Tag | None = None

	def convert(self) -> str:
		self.parse_body()
		self.merge_sections()
		self.remove_book_title_duplication()
		body = self.soup.find("body")
		body.insert(0, self.landing())
		self.prepare_images()
		for title in self.soup.find_all("title"):
			title.name = "header"
		for subtitle in self.soup.find_all("subtitle"):
			subtitle.name = "header"
		for empty_line in self.soup.find_all("empty-line"):
			empty_line.name = "br"
		for p in self.soup.find_all("p"):
			if not p.contents:
				p.extract()
			elif len(p.text) < 10 and p.text.replace(" ", "").strip() in ("***", "****", "*****", "+++", "++++", "+++++"):
				p.string = "* * *"
				p.wrap(self.soup.new_tag("header"))
		for p in self.soup.select("p > p"):
			p.unwrap()
		for strong in self.soup.find_all("strong"):
			if not strong.contents:
				strong.extract()
			else:
				strong.name = "b"
		for section in self.soup.find("body").find_all("section"):
			for item in section:
				if isinstance(item, NavigableString):
					if item.text.strip():
						item.wrap(self.soup.new_tag("p"))
					else:
						item.extract()
			while len(section.contents) and section.contents[-1].name == "br":
				section.contents[-1].extract()
			while len(section.contents) and section.contents[0].name == "br":
				section.contents[0].extract()
			if len(section.contents) > 1 and section.contents[0].name == "header":
				while len(section.contents) > 1 and section.contents[1].name == "br":
					section.contents[1].extract()
		for a in self.soup.find_all("a"):
			href = self.get_href(a)
			if href:
				a.attrs = {"href": href, "target": "_blank"}
		for a in body.select("a[href^='#']"):
			note = self.soup.find(id=a["href"].split("#")[1])
			if not note: continue
			parent = a.parent
			if parent.name == "p":
				parent.name = "div"
			a.name = "div"
			a.attrs = {"class": "noteContainer"}
			note.name = "div"
			note.attrs = {"class": "note"}
			a.append(note)
		for emphasis in body.find_all("emphasis"):
			if not len(emphasis.contents):
				continue
			space_before = self.check_first_character(emphasis) or self.check_previous_character(emphasis)
			space_after = self.check_last_character(emphasis) or self.check_next_character(emphasis)
			if len(emphasis.text.strip()) == 1 and space_before and space_after:
				self.remove_previous_character(emphasis)
				self.remove_next_character(emphasis)
			elif space_before and not space_after:
				emphasis.append(" ")
			elif not space_before and space_after:
				emphasis.insert(0, " ")
		body.name = "div"
		body.attrs = {"class": "book", "title": self.book.name}
		return str(body).replace("<br></br>", "<br>").replace("&amp;", "&")

	def parse_body(self):
		content = self.soup.new_tag("body")
		body = self.soup.find("body")
		for section in body.find_all("section", recursive=True):
			content.append(section)
		body.replace_with(content)
		if len(body.get_text(strip=True)) > 0:
			body.name = "section"
			content.insert(0, body)

	def merge_sections(self):
		body = self.soup.find("body")
		empty_sections = []
		for section in body.find_all("section", recursive=False):
			if all(tag.name in ("title", "subtitle") for tag in section.find_all(recursive=False)):
				empty_sections.append(section)
				continue
			while len(empty_sections) > 0:
				tag = empty_sections.pop()
				section.insert(0, tag)
				tag.unwrap()

	def remove_book_title_duplication(self):
		book_title = self.soup.find("book-title")
		first_tag = self.soup.select_one("body section title")
		if not book_title or not first_tag:
			return
		if first_tag.text.strip().replace("ё", "е") == book_title.text.strip().replace("ё", "е"):
			first_tag.extract()

	def landing(self) -> Tag:
		landing = self.soup.new_tag("section")
		book_title = self.soup.find("book-title")
		if book_title:
			header = self.soup.new_tag("header")
			header.string = book_title.text.strip()
			landing.append(header)
		annotation = self.soup.find("annotation")
		if annotation:
			landing.append(annotation)
			annotation.unwrap()
		coverpage = self.coverpage_content() or self.soup.find("binary")
		if coverpage:
			landing.append(self.soup.new_tag("br"))
			coverpage_container = self.soup.new_tag("div")
			coverpage_container.attrs = {"class": "coverpage"}
			coverpage_container.append(self.soup.new_tag("img",
				src=f"data:image/png;base64, {coverpage.text.strip()}"))
			landing.append(coverpage_container)
		return landing

	def coverpage_content(self) -> Tag | None:
		if not self.__coverpage:
			if coverpage := self.soup.find("coverpage"):
				image = coverpage.find("image")
				if image and self.get_href(image):
					self.__coverpage = self.soup.find("binary", {"id": self.get_href(image)[1:]})
		return self.__coverpage

	def prepare_images(self):
		for tag in self.soup.find_all("image"):
			if not self.get_href(tag):
				continue
			image_id = self.get_href(tag)[1:]
			binary = self.soup.find("binary", id=image_id)
			if not binary:
				continue
			content = binary.text
			image = self.soup.new_tag("img", src=f"data:image/png;base64, {content}")
			expand = self.soup.new_tag("img")
			tag.name = "div"
			tag.attrs = {"class": "image"}
			tag.append(image)
			tag.append(expand)

	def collect_images(self) -> Tag:
		container = self.soup.new_tag("div")
		container.attrs = {"class": "bookImages", "title": self.book.name}
		for binary in self.soup.find_all("binary"):
			container.append(self.soup.new_tag("img", src=f"data:image/png;base64, {binary.text}"))
		return container

	def check_last_character(self, emphasis: Tag) -> bool:
		content = emphasis.contents[-1]
		if not content or not isinstance(content, NavigableString):
			return True
		return bool(re.match(r"\s", content[-1]))

	def check_next_character(self, emphasis: Tag) -> bool:
		next_element = self.get_next_string(emphasis)
		if next_element is None:
			return True
		return bool(re.match(r"(\s|\.|,|:|;|-|—|!|\?)", next_element[0]))

	def check_first_character(self, emphasis: Tag) -> bool:
		content = emphasis.contents[0]
		if not content or not isinstance(content, NavigableString):
			return True
		return bool(re.match(r"(\s|\.|,|:|;|-|—|!|\?)", content[0]))

	def check_previous_character(self, emphasis: Tag) -> bool:
		previous_element = self.get_previous_string(emphasis)
		if previous_element is None:
			return True
		return bool(re.match(r"\s", previous_element[-1]))

	def remove_previous_character(self, emphasis: Tag):
		previous_element = self.get_previous_string(emphasis)
		if previous_element is not None:
			previous_element.replace_with(previous_element[:-1])

	def remove_next_character(self, emphasis: Tag):
		next_element = self.get_next_string(emphasis)
		if next_element is not None:
			next_element.replace_with(next_element[1:])

	@staticmethod
	def get_previous_string(tag: Tag) -> NavigableString | None:
		previous_element_index = tag.parent.index(tag) - 1
		if previous_element_index >= 0:
			previous_element = tag.parent.contents[previous_element_index]
			if isinstance(previous_element, NavigableString):
				return previous_element

	@staticmethod
	def get_next_string(tag: Tag) -> NavigableString | None:
		next_element_index = tag.parent.index(tag) + 1
		if next_element_index < len(tag.parent.contents):
			next_element = tag.parent.contents[next_element_index]
			if isinstance(next_element, NavigableString):
				return next_element

	@staticmethod
	def get_href(tag: Tag) -> str | None:
		for key, value in tag.attrs.items():
			if ":href" in key:
				return value

class Cycle:

	instances: dict[str, 'Cycle'] = {}

	@classmethod
	def get(cls, path: str) -> 'Cycle':
		if path not in cls.instances:
			cls.instances[path] = Cycle(path)
		return cls.instances[path]

	def __init__(self, path: str):
		self.path: str = path
		self.valid: bool = self.path.startswith(books_storage)
		self.name: str = os.path.basename(self.path)
		self.htmlfolder: str = os.path.join(root, "pages", self.name)
		self.htmlpath: str = os.path.join(self.htmlfolder, f"{self.name}.html")
		if not os.path.exists(self.htmlfolder):
			os.mkdir(self.htmlfolder)
		self.__infopath: str = os.path.join(self.htmlfolder, "info.json")
		self.__info: dict | None = None
		self.__books: list[Book] | None = None
		self.__htmldata = None

	def info(self) -> dict | None:
		if not self.__info and os.path.exists(self.__infopath):
			with open(self.__infopath, "r", encoding="utf-8") as f:
				self.__info = json.load(f)
		return self.__info

	def set_info(self):
		if not self.valid:
			return
		info = {"timestamp": time.time(), "books": [book.name for book in self.books()]}
		with open(self.__infopath, "w", encoding="utf-8") as f:
			json.dump(info, f, ensure_ascii=False, indent=2)

	def books(self) -> list[Book]:
		if not self.__books and self.valid:
			self.__books = [
				Book.get(os.path.join(self.path, name))
				for name in os.listdir(self.path)
				if Book.valid(os.path.join(self.path, name))
			]
			self.__books.sort(key=lambda b: float(re.search("(\\d+(?:\\.\\d+)?)", b.path).group()))
		return self.__books or []

	def images(self) -> str:
		return "".join(map(lambda book: book.images(), self.books()))

	def html(self) -> str:
		if not self.__htmldata:
			self.__htmldata = "".join(map(lambda book: book.html(), self.books()))
		return self.__htmldata

class Watcher:

	__files = ["main.py", "webapp/public/index.html"]

	__last_local_change_time: int = None
	__git_inited: bool = None
	__last_commit_time: int = None

	__cache = {}

	@classmethod
	def should_generate(cls, cycle: Cycle) -> bool:
		if cycle.path not in cls.__cache:
			cls.__cache[cycle.path] = cls.__should_generate(cycle)
		return cls.__cache[cycle.path]

	@classmethod
	def __should_generate(cls, cycle: Cycle) -> bool:
		if not cycle.valid or not cycle.info():
			return True
		if not os.path.exists(cycle.htmlpath):
			return True
		if cycle.info()["timestamp"] < cls.__last_local_change():
			return True
		if cls.__git_changes(cycle.info()["timestamp"]):
			return True
		if cycle.info()["books"] != [book.name for book in cycle.books()]:
			return True
		for book in cycle.books():
			if cycle.info()["timestamp"] < os.path.getmtime(book.path):
				return True
		return False

	@classmethod
	def __last_local_change(cls) -> int:
		if not cls.__last_local_change_time:
			cls.__last_local_change_time = max(*[os.path.getmtime(file) for file in cls.__files])
		return cls.__last_local_change_time

	@classmethod
	def __is_git_inited(cls) -> bool:
		if cls.__git_inited is None:
			cls.__git_inited = len(os.popen("git status").read()) > 0
		return cls.__git_inited

	@classmethod
	def __last_commit(cls) -> int | None:
		if not cls.__is_git_inited():
			return None
		if cls.__last_commit_time is None:
			cls.__last_commit_time = int(os.popen('git log -1 --format="%at"').read())
		return cls.__last_commit_time

	@classmethod
	def __git_changes(cls, since: Union[int, float]) -> bool | None:
		if not cls.__is_git_inited():
			return None
		output = os.popen(f'git log --pretty=format:"" --name-only --since={since}').read()
		changes = [line for line in output.split("\n") if line]
		return any(file in cls.__files for file in changes)

def fb2_to_html(bookpath: str) -> str:
	if not Book.valid(bookpath):
		raise Exception("Invalid file: this is not a book")
	book = Book.get(bookpath)
	with open(os.path.join(root, "webapp/build/index.html"), "r", encoding="utf-8") as template:
		with open(book.htmlpath, "w", encoding="utf-8") as htmlfile:
			htmlfile.write(template.read().format(
				title=book.name,
				cycledata=book.cycle.html() or book.html(),
				images=book.cycle.images() or book.images()))
	return book.htmlpath

def cycle_to_html(bookpath: str) -> str:
	if not Book.valid(bookpath):
		raise Exception("Invalid file: this is not a book")
	book = Book.get(bookpath)
	if not Watcher.should_generate(book.cycle):
		return book.cycle.htmlpath
	with open(os.path.join(root, "webapp/build/index.html"), "r", encoding="utf-8") as template:
		with open(book.cycle.htmlpath, "w", encoding="utf-8") as htmlfile:
			htmlfile.write(template.read().format(title="",
				cycledata=book.cycle.html(),
				images=book.cycle.images()))
	book.cycle.set_info()
	return book.cycle.htmlpath

def send_telegram_message(chat_id: int, text: str) -> bool:
	if not bot_token:
		return False
	try:
		url = f"https://api.telegram.org/bot{bot_token}/sendMessage"
		params = json.dumps({"chat_id": str(chat_id), "text": text}).encode("utf8")
		request = urllib.request.Request(url, data=params, headers={"content-type": "application/json"})
		return urllib.request.urlopen(request)
	except Exception:
		return False

def main(filepath: str):
	try:
		path = cycle_to_html(filepath) if filepath.startswith(books_storage) else fb2_to_html(filepath)
		if platform.system() == "Windows":
			os.system(f'start "" "{path}"')
		else:
			os.system(f'open "{path}"')
	except Exception:
		if not send_telegram_message(telegram_id, traceback.format_exc()):
			with open(os.path.join(root, "exception.txt"), "w") as f:
				f.write(traceback.format_exc())
		raise Exception("Unexpected exception: check Telegram messages or exception.txt in the project root directory")

if __name__ == '__main__':
	if len(sys.argv) > 1:
		main(sys.argv[1])
	else:
		main(input("book path: "))
