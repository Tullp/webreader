import React, {Fragment, useContext} from 'react';
import styles from './chapter.module.scss';
import {AppContext} from './appcontext';
import {Header} from './header/header';
import {Reader} from './reader/reader';
import {Controls} from './controls/controls';
import {SettingsContext} from './settings/settingscontext';

/**
 * @returns A React fragment containing the Header, Reader, and Controls components.
 */
export const Chapter = () => {
  const {chapter} = useContext(AppContext);
  const {settings} = useContext(SettingsContext);
  const isLanding = chapter.number == 0;
  return <Fragment>
    <Header/>
    <Reader content={chapter.content.innerHTML} cls={isLanding && styles.emphasis}
      customSettings={{emphasisRight: isLanding ? false : settings.emphasisRight}}/>
    <Controls/>
  </Fragment>;
};
