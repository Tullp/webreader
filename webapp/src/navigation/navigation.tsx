import React, {useContext} from 'react';
import styles from './navigation.module.scss';
import {AppContext} from '../appcontext';

/** Component for displaying current chapter and navigating between chapters and books. */
export const Navigation = () => {
  const {chapter, next, previous} = useContext(AppContext);
  return <div className={styles.container}>
    <button onClick={previous}><span style={{transform: 'scale(-1, 1)'}}>➜</span></button>
    <div className={styles.title} title={chapter.title}>{chapter.title}</div>
    <button onClick={next}><span>➜</span></button>
  </div>;
};

