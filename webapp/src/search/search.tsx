import React, {useCallback, useContext, useEffect, useRef, useState} from 'react';
import styles from './search.module.scss';
import {CaretBack} from 'react-ionicons'
import {cn, keyBinding} from '../utils';
import {Reader} from '../reader/reader';
import {AppContext, iBook, iChapter} from '../appcontext';
import {iSettings, SettingsContext} from '../settings/settingscontext';
import {iInputRef, Input} from '../input/input';
import chevronForward from './chevron-forward-outline.svg';

interface iSearchResults {
  results: string;
  onLoad: (container: HTMLDivElement) => (()=>void)[];
}

/**
 * Component to display search results.
 * @param {Object} props - The props object.
 * @param {string} props.results - The search results to display.
 * @param {function} props.onLoad - The callback function to handle content load.
 * @returns The rendered search results component.
 */
const SearchResults = ({results, onLoad}: iSearchResults) => {
  return <div className={cn(styles.searchResultsContainer, !!results && styles.opened)}>
    <div className={cn(styles.results)}>
      {!!results && <Reader content={results} onLoad={onLoad}/>}
    </div>
  </div>;
};

interface iSearchProps {
  onSearch: (query: string) => void;
  results: string;
  query: string;
  setQuery: (query: string) => void;
  onClose: () => void;
  inputRef: React.RefObject<iInputRef>;
}

/**
 * Search input component.
 *
 * This component provides a search input field with various functionalities such as
 * searching, clearing the search query, focusing, and blurring.
 *
 * @param {Object} props - The props object containing the following properties:
 * @param {function} props.onSearch - The callback function to execute when the search performing.
 * @param {string} props.results - Indicates whether search results are available.
 * @param {string} props.query - The current search query.
 * @param {function} props.setQuery - The function to set the search query.
 * @param {function} props.onClose - The function to execute when the search surfing is finish.
 * @param {React.RefObject<iInputRef>} props.inputRef - The reference object for this component.
 *
 * @returns The rendered search component.
 */
const SearchInput = ({onSearch, results, query, setQuery, onClose, inputRef}: iSearchProps) => {
  const {setHeaderBlocker} = useContext(AppContext);
  const onBlur = () => {
    setHeaderBlocker('searchQuery', false);
    setHeaderBlocker('search', !!results);
  }
  return <div className={styles.searchContainer}>
    {!!results && <div className={styles.close} onClick={onClose}>
      <CaretBack color="#bdb7af" height="26px" width="26px"/>
    </div>}
    <Input value={query} placeholder="Поиск" width={225} ref={inputRef} searchIconSize={18}
      onApply={onSearch} onChange={setQuery} onBlur={onBlur} onFocus={() => setHeaderBlocker('searchQuery', true)}/>
  </div>;
};

interface iBookSearchResult {
  query: string;
  book: iBook;
  blocks: iBookSearchResultBlock[];
}

interface iBookSearchResultBlock {
  chapter: iChapter;
  paragraphs: Node[];
}

const bookSearch = (book: iBook, query: string, settings: iSettings) => {

  const result: iBookSearchResult = {query, book, blocks: []};

  const contains = (paragraph: HTMLElement) => {
    if (!paragraph) return false;
    if (settings.wordBoundary)
      return new RegExp(`(^|\\s|[^a-zA-Zа-яА-Я0-9_])${query}(\\s|[^a-zA-Zа-яА-Я0-9_]|$)`,
        !settings.caseSensitivity ? 'i' : '').test(paragraph.innerText);
    if (settings.caseSensitivity)
      return paragraph.innerText.indexOf(query) >= 0;
    return paragraph.innerText.toLowerCase().indexOf(query.toLowerCase()) >= 0;
  }

  // iterate every chapter
  for (let chapterNumber = 0; chapterNumber < book.chapters.length; chapterNumber++) {

    const paragraphs = book.chapters[chapterNumber].content.querySelectorAll<HTMLElement>('& > *');
    let last_paragraph = -1;

    // iterate every line in chapter
    for (let i = 0; i < paragraphs.length; i++) {

      if (!contains(paragraphs[i]))
        continue;

      const block: iBookSearchResultBlock = {chapter: book.chapters[chapterNumber], paragraphs: []};

      // go forward and take a continuous sequence of occurrences. variable last is included.
      let start = i;
      let last = i;
      while (last + 1 < paragraphs.length) {
        if (!contains(paragraphs[last + 1]))
          break;
        last += 1;
      }

      // go forward until reach at least 1000 symbols and add accordingly paragraphs to another side.
      let previous_paragraphs_length = 0;
      let next_paragraphs_length = 0;
      do {

        const startExpand = start > 0 && start - 1 > last_paragraph && previous_paragraphs_length < 1000;
        const endExpand = last + 1 < paragraphs.length;
        if (!startExpand && !endExpand)
          break;

        if (startExpand) {
          start -= 1;
          previous_paragraphs_length += paragraphs[start].innerText.length;
        }
        if (endExpand) {
          last += 1;
          next_paragraphs_length += paragraphs[last].innerText.length;
          if (contains(paragraphs[last]))
            next_paragraphs_length = 0;
        }
      } while (next_paragraphs_length < 1000 || contains(paragraphs[last + 1]))

      // finish a block sequence
      last_paragraph = last;
      for (let j = start; j <= last; j++)
        block.paragraphs.push(paragraphs[j].cloneNode(true));
      result.blocks.push(block);
      i = last;
    }
  }

  return result;
};

const chevronElement = (book: iBook, chapter: iChapter) => {
  const container = document.createElement('div');
  container.className = styles.chevron;
  container.dataset.book = book.number.toString();
  container.dataset.chapter = chapter.number.toString();
  const chevron = document.createElement('img');
  chevron.src = chevronForward;
  container.appendChild(chevron);
  return container;
};

const HTMLBookSearchResult = (book: iBook, query: string, books: iBook[], settings: iSettings) => {

  const result = bookSearch(book, query, settings);

  if (result.blocks.length === 0) return;

  const bookTitle = document.createElement('header');
  bookTitle.innerHTML = result.book.title;

  const bookResults = document.createElement('div');
  if (books.length > 1 && !(settings.expandFirstBook && result.book.number === 0))
    bookResults.style.display = 'none';

  result.blocks.forEach((block) => {
    const blockContainer = document.createElement('div');
    blockContainer.className = styles.block;
    block.paragraphs.forEach((paragraph) => {
      blockContainer.append(paragraph);
    });
    const regex = new RegExp(`(${result.query})`, settings.caseSensitivity ? 'g' : 'ig');
    blockContainer.innerHTML = blockContainer.innerHTML.replace(regex, '<mark>$1</mark>');
    blockContainer.append(chevronElement(result.book, block.chapter));
    bookResults.append(blockContainer);
  });

  return bookTitle.outerHTML + bookResults.outerHTML;
};

/**
 * Search hook that allows searching text occurrences in all books and chapters within a context.
 * @returns Returns an object containing searchElement and resultsContainer components.
 */
export const useSearch = () => {
  const {books, setHeaderBlocker} = useContext(AppContext);
  const {settings} = useContext(SettingsContext);
  const inputRef = useRef<iInputRef>(null);
  const [query, setQuery] = useState<string>('');
  const [results, setResults] = useState<string>('');

  const onSearch = useCallback((query: string) => {
    if (!query || query.length < 2) return;
    setHeaderBlocker('search', true);
    setResults('');
    books.forEach((book) => {
      const resultString = HTMLBookSearchResult(book, query, books, settings);
      if (resultString) setResults((p) => p + resultString);
    });
  }, [books, query, settings]);

  const onLoad = (container: HTMLDivElement) => {
    const cleaners: (()=>void)[] = []
    const books = container.querySelectorAll<HTMLDivElement>('& > div');
    container.querySelectorAll<HTMLDivElement>('& > header').forEach((header, i) => {
      const listener = () => {
        books[i].style.display = books[i].style.display === 'none' ? 'block' : 'none';
      };
      header.addEventListener('click', listener);
      cleaners.push(() => header.removeEventListener('click', listener));
    });
    container.querySelectorAll<HTMLDivElement>(`.${CSS.escape(styles.chevron)}`).forEach((chevron) => {
      const listener = () => {
        const book = chevron.dataset.book;
        const chapter = chevron.dataset.chapter;
        window.open(`${window.location.href}#${encodeURIComponent(JSON.stringify({book, chapter}))}`);
      };
      chevron.addEventListener('click', listener);
      cleaners.push(() => chevron.removeEventListener('click', listener));
    });
    return cleaners;
  };

  const onClose = () => {
    setHeaderBlocker('search', false);
    setResults('');
    setQuery('');
  };

  const searchElement = <SearchInput onSearch={onSearch} results={results}
    query={query} setQuery={setQuery} onClose={onClose} inputRef={inputRef}/>;
  const searchResultsContainer = <SearchResults onLoad={onLoad} results={results}/>;

  useEffect(() => {
    const listener = (event: KeyboardEvent) => {
      if (keyBinding('cmd+shift+f', event)) {
        event.preventDefault();
        event.stopPropagation();
        if (window.getSelection()?.toString().length) {
          setQuery(window.getSelection()!.toString());
          onSearch(window.getSelection()!.toString());
        } else {
          setHeaderBlocker('search', true);
          inputRef.current!.focus();
        }
      }
      if (keyBinding('escape', event))
        onClose();
    };
    document.addEventListener('keydown', listener);
    return () => document.removeEventListener('keydown', listener);
  }, []);

  return {searchElement, searchResultsContainer};
}
