import React, {Fragment, useContext, useEffect, useMemo, useRef} from 'react';
import './reader.scss';
import {SettingsContext} from '../settings/settingscontext';
import {cn} from '../utils';
import chevronExpand from './chevron-expand-outline.svg';
import chevronCollapse from './chevron-collapse-outline.svg';
import {useFullscreenImage} from '../fullscreen-image/fullscreen-image';

interface iProps {
  content: string;
  cls?: string;
  onLoad?: (container: HTMLDivElement) => (() => void)[];
  customSettings?: { [p: string]: boolean };
}

/**
 * Component that render strings of the html code.
 *
 * @param {Object} props - The props object containing the required parameters.
 * @param {string} props.content - The HTML content to be rendered.
 * @param {function} props.onLoad - The function called when the component is loaded.
 *                               It takes a container element as the argument and
 *                               returns an array of functions that can be called to unsubscribe any listeners
 *                               or perform cleanup tasks when the component is unmounted.
 * @returns The React element representing the rendered content.
 */
export const Reader = ({content, cls, onLoad, customSettings}: iProps) => {
  const {settings: globalSettings} = useContext(SettingsContext);
  const settings = useMemo(() => {
    return Object.assign({}, globalSettings, customSettings || {});
  }, [globalSettings, customSettings]);
  const contentRef = useRef<HTMLDivElement>(null);
  const {FullscreenImage, setExpandedImage} = useFullscreenImage();
  useEffect(() => {
    if (!onLoad) return;
    const cleaners = onLoad(contentRef.current!);
    return () => cleaners.forEach((cleaner) => cleaner());
  }, [content]);
  // images
  useEffect(() => {
    const cleaners: (() => void)[] = [];
    contentRef.current!.querySelectorAll<HTMLElement>('.image').forEach((container) => {
      const image = container.querySelector<HTMLImageElement>('img:nth-child(1)')!;
      const button = container.querySelector<HTMLImageElement>('img:nth-child(2)')!;
      button.src = chevronExpand;
      let collapsed = true;
      let scrollBeforeExpand = 0;
      let scrollAfterExpand = 0;
      const listener = () => {
        if (settings.openFullscreenImages)
          return setExpandedImage(image);
        button.classList.toggle('active');
        if (collapsed) {
          image.style.marginTop = '50px';
          image.style.maxWidth = '100%';
          image.style.maxHeight = 'calc(100vh - 200px)';
          button.src = chevronCollapse;
          collapsed = false;
          scrollBeforeExpand = window.scrollY;
          scrollAfterExpand = button.getBoundingClientRect().top + window.scrollY - 50;
          setTimeout(() => window.scroll({top: scrollAfterExpand, behavior: 'smooth'}), 200);
        } else {
          image.style.marginTop = '0';
          image.style.maxWidth = '40px';
          image.style.maxHeight = '40px';
          button.src = chevronExpand;
          collapsed = true;
          if (Math.abs(window.scrollY - scrollAfterExpand) <= 100)
            setTimeout(() => window.scroll({top: scrollBeforeExpand, behavior: 'smooth'}), 200);
        }
      };
      button.addEventListener('click', listener);
      cleaners.push(() => button.removeEventListener('click', listener));
    });
    return () => cleaners.forEach((cleaner) => cleaner());
  }, [content, settings.openFullscreenImages]);
  return <Fragment>
    <div className={cn('content', settings.emphasisRight && 'emphasisRight', cls)}
      ref={contentRef} dangerouslySetInnerHTML={{__html: content}}/>
    {FullscreenImage}
  </Fragment>;
};
