import React, {useContext, useEffect, useRef, useState} from 'react';
import styles from './images.module.scss';
import {AppContext, iBook} from '../appcontext';
import {Subwindow} from '../subwindow/subwindow';
import {ImagesOutline as ImagesIcon} from 'react-ionicons';
import {cn, keyBinding} from '../utils';
import {SettingsContext} from '../settings/settingscontext';
import {useFullscreenImage} from '../fullscreen-image/fullscreen-image';

export const Images = () => {
  const {books, book, images, setHeaderBlocker} = useContext(AppContext);
  const {settings} = useContext(SettingsContext);
  const [opened, setOpened] = useState<boolean>(false);
  const [selectedBook, setSelectedBook] = useState<iBook>();
  const imagesRef = useRef<HTMLDivElement>(null);
  const resetScroll = useRef<boolean>(false);
  const {FullscreenImage, setExpandedImage} = useFullscreenImage();
  const onOpen = (e?: React.MouseEvent) => {
    e?.preventDefault();
    e?.stopPropagation();
    setOpened(true);
    setHeaderBlocker('images', true);
    document.body.style.overflowY = 'hidden';
  };
  const onClose = () => {
    setOpened(false);
    setHeaderBlocker('images', false);
    if (!settings.rememberImagesTab)
      setSelectedBook(undefined);
    document.body.style.removeProperty('overflow-y');
  }
  useEffect(() => {
    if (opened && resetScroll.current) {
      resetScroll.current = false;
      imagesRef.current!.scrollTo(0, 0);
    }
    const listener = (e: KeyboardEvent) => {
      if (!opened && keyBinding('cmd+i', e)) {
        e.preventDefault();
        e.stopPropagation();
        onOpen();
      } else if (opened && keyBinding('escape', e)) {
        e.preventDefault();
        e.stopPropagation();
        onClose();
      }
    }
    document.addEventListener('keydown', listener);
    return () => document.removeEventListener('keydown', listener);
  }, [opened]);
  // fill images container
  useEffect(() => {
    if (!imagesRef.current)
      return;
    const cleaners: (() => void)[] = [];
    imagesRef.current.innerHTML = '';
    images[selectedBook ? selectedBook.number : book.number].images.forEach((image) => {
      const element = imagesRef.current!.appendChild(image.content.cloneNode(true)) as HTMLElement;
      const listener = () => setExpandedImage(image.content);
      element.addEventListener('click', listener);
      cleaners.push(() => element.removeEventListener('click', listener));
    });
    return () => cleaners.forEach((cleaner) => cleaner());
  }, [book, selectedBook, imagesRef]);
  // reset scroll
  useEffect(() => {
    if (!selectedBook)
      resetScroll.current = true;
  }, [book, selectedBook]);
  // scroll to top on book select
  useEffect(() => {
    imagesRef.current?.scrollTo(0, 0);
  }, [selectedBook]);
  return <div className={styles.container}>
    <div className={styles.openImages} onClick={onOpen}>
      <ImagesIcon color="#BDB7AF" width={22} height={22}/>
    </div>
    <Subwindow opened={opened} side="center" width={850} maxHeight={750} onClose={onClose}
        noCloseSelectors={[`.${CSS.escape(styles.imageLayer)}`]}>
      <div className={styles.window}>
        <div className={styles.books}>
          {books.map((b) => <div className={cn(styles.book, b === (selectedBook || book) && styles.active)}
              onClick={() => setSelectedBook(b)}>
            <p>{b.title}</p>
          </div>)}
        </div>
        <div className={styles.images} ref={imagesRef}/>
      </div>
      {FullscreenImage}
    </Subwindow>
  </div>;
};
