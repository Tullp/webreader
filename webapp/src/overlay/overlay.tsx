import React from 'react';
import styles from './overlay.module.scss';
import {cn} from '../utils';

interface iOverlayProps {
  children: React.ReactNode;
  opened: boolean;
  onOverlayClick: () => void;
}


/**
 * A component that overlays its children on top of a background layer.
 *
 * @param {Object} props - The props for the Overlay component.
 * @param {ReactNode} props.children - The content to display within the overlay.
 * @param {boolean} props.opened - Whether the overlay is currently opened.
 * @param {function} props.onOverlayClick - The callback function to handle overlay click events.
 * @returns The rendered Overlay component.
 */
export const Overlay = ({children, opened, onOverlayClick}: iOverlayProps) => {
  return <div className={cn(styles.container, opened && styles.opened)}>
    <div className={styles.overlay} onClick={onOverlayClick}/>
    {children}
  </div>;
};
