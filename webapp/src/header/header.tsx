import React, {Fragment, useContext, useEffect, useState} from 'react';
import styles from './header.module.scss';
import {Navigation} from '../navigation/navigation';
import {useSearch} from '../search/search';
import {Settings} from '../settings/settings';
import {SidePanel} from '../sidepanel/sidepanel';
import {Bookmarks} from '../bookmarks/bookmarks';
import {AppContext} from '../appcontext';
import {cn} from '../utils';
import {Images} from '../images/images';
import {SettingsContext} from '../settings/settingscontext';

/**
 * Represents the header component of the application.
 * @returns The rendered header component.
 */
export const Header = () => {
  const {book, chapter, headerBlockers, setHeaderBlocker} = useContext(AppContext);
  const {settings} = useContext(SettingsContext);
  const [opened, setOpened] = useState<boolean>(true);
  const [sidePanelOpened, setSidePanelOpened] = useState<boolean>(false);
  const [sidePanelOpenedByMouse, setSidePanelOpenedByMouse] = useState<boolean>(false);
  const {searchElement, searchResultsContainer} = useSearch();
  useEffect(() => {
    setOpened(Object.values(headerBlockers).some(block => block));
  }, [headerBlockers]);
  useEffect(() => {
    const scrollListener = () => {
      setHeaderBlocker('scroll', window.scrollY <= 100);
    };
    const mouseListener = (e: MouseEvent) => {
      setHeaderBlocker('mouse', e.y <= 54);
    };
    document.addEventListener('scroll', scrollListener);
    document.addEventListener('mousemove', mouseListener);
    return () => {
      document.removeEventListener('scroll', scrollListener);
      document.removeEventListener('mousemove', mouseListener);
    };
  }, []);
  const [progress, setProgress] = useState<number>(0);
  const updateProgress = () => {
    const previousChapters = book.chapters
      .slice(0, chapter.number)
      .reduce((a, ch) => a + ch.size, 0);
    const scroll = document.documentElement.scrollTop;
    const height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    const currentChapter = chapter.size * scroll / height || 0;
    setProgress(Math.round((previousChapters + currentChapter) / book.size * 1000) / 10);
  };
  useEffect(() => {
    updateProgress();
    const listener = () => updateProgress();
    document.addEventListener('scroll', listener);
    return () => document.removeEventListener('scroll', listener);
  }, [chapter]);
  useEffect(() => {
    if (sidePanelOpened) return;
    const listener = (e: MouseEvent) => {
      if (settings.sidePanelWideOpeningRange ? e.x <= 200 : e.x === 0) {
        setSidePanelOpened(true);
        setSidePanelOpenedByMouse(true);
        setHeaderBlocker('sidepanel', true);
      }
    };
    document.addEventListener('mousemove', listener);
    return () => document.removeEventListener('mousemove', listener);
  }, [sidePanelOpened, settings.sidePanelWideOpeningRange]);
  return <Fragment>
    <div className={cn(styles.container, opened && styles.opened)}>
      <div className={styles.header}>
        <div className={styles.left}>
          <Bookmarks/>
          <button className={styles.openSidePanel} onClick={() => {
            setSidePanelOpened(true);
            setHeaderBlocker('sidepanel', true);
          }}>Содержание</button>
          <p className={styles.progress}>{progress}%</p>
        </div>
        <div className={styles.navigator}><Navigation/></div>
        <div className={styles.right}>
          <Images/>
          {searchElement}
          <Settings/>
        </div>
      </div>
    </div>
    <SidePanel opened={sidePanelOpened} onClose={() => {
      setSidePanelOpened(false);
      setSidePanelOpenedByMouse(false);
      setHeaderBlocker('sidepanel', false);
    }} sidePanelOpenedByMouse={sidePanelOpenedByMouse}/>
    {searchResultsContainer}
  </Fragment>;
};
