import React, {useEffect} from 'react';
import {AppContextProvider} from './appcontext';
import {SettingsContextProvider} from './settings/settingscontext';
import {ThemeContextProvider} from './theme/themecontext';
import {Chapter} from './chapter';

/**
 * Prevents page scrolling on the space key press.
 */
const usePreventScrollOnSpaceKeyPress = () => {
  useEffect(() => {
    const listener = (event: KeyboardEvent) => {
      if (event.key === ' ' && event.target === document.body)
        event.preventDefault();
    };
    document.body.addEventListener('keydown', listener);
    return () => document.body.removeEventListener('keydown', listener);
  }, []);
}

export const App = () => {
  usePreventScrollOnSpaceKeyPress();
  return <AppContextProvider>
    <ThemeContextProvider>
      <SettingsContextProvider>
        <Chapter/>
      </SettingsContextProvider>
    </ThemeContextProvider>
  </AppContextProvider>;
};
