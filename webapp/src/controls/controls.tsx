import React, {useContext} from 'react';
import styles from './controls.module.scss';
import {AppContext} from '../appcontext';

interface iNavigationButton {
  action: () => void;
  text: string;
}

const NavigationButton = ({action, text}: iNavigationButton): React.JSX.Element => {
  return <a onClick={action}>{text}</a>;
}

/** Component for navigating between chapters and books. */
export const Controls = () => {

  const {books, book, next, previous, chapter} = useContext(AppContext);
  const isLastBook = book.number === books.length - 1;
  const isLastChapter = chapter.number === book.chapters.length - 1;
  const isFirstBook = book.number === 0;
  const isFirstChapter = chapter.number === 0;

  return <div className={styles.controls}>
    {!isFirstChapter && <NavigationButton action={previous} text="Предыдущая глава"/>}
    {isFirstChapter && !isFirstBook && <NavigationButton action={previous} text="Предыдущая книга"/>}
    {!isLastChapter && <NavigationButton action={next} text="Следующая глава"/>}
    {isLastChapter && !isLastBook && <NavigationButton action={next} text="Следующая книга"/>}
  </div>;
};
