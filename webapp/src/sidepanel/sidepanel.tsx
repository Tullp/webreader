import React, {useContext, useEffect, useMemo, useRef, useState} from 'react';
import styles from './sidepanel.module.scss';
import {AppContext} from '../appcontext';
import {Overlay} from '../overlay/overlay';
import {cn} from '../utils';

interface iSidePanel {
  opened: boolean;
  onClose: () => void;
  sidePanelOpenedByMouse: boolean;
}

/**
 * Component for displaying book's chapters and all books in cycle.
 *
 * @param {Object} props - The component props.
 * @param {boolean} props.opened - Indicates whether the side panel is opened or not.
 * @param {function} props.onClose - Callback function to close the side panel.
 * @param {boolean} props.sidePanelOpenedByMouse - Indicates whether the side panel was opened by mouse move or not.
 * @returns The rendered side panel component.
 */
export const SidePanel = ({opened, onClose, sidePanelOpenedByMouse}: iSidePanel) => {
  const panelRef = useRef<HTMLDivElement>(null);
  const coverpageRef = useRef<HTMLImageElement>(null);
  const {cycleName, books, book, chapter, setChapter, setBook} = useContext(AppContext);
  const [transformed, setTransformed] = useState<boolean>(true);
  const [hoverable, setHoverable] = useState<boolean>(false);
  const [coverpageWidth, setCoverpageWidth] = useState<number>(0);
  const cycleSize = books.reduce((sum, b) => sum + b.size, 0);
  const coverpageContent = useMemo(() => {
    return book.chapters[0].content.querySelector<HTMLImageElement>('.coverpage img')?.src;
  }, [book]);
  const scrollToActiveChapter = () => {
    const list = panelRef.current!.querySelector<HTMLDivElement>(`.${styles.chaptersList}`)!;
    const beforeActive = list.querySelector<HTMLDivElement>(`div:has(+.${styles.active})`);
    if (beforeActive != null)
      list.scrollTo(0, beforeActive.offsetTop - list.offsetTop - 10);
    else list.scrollTo(0, 0);
  };
  useEffect(() => {
    const panel = panelRef.current!;
    const startListener = (event: TransitionEvent) => {
      if (event.propertyName === 'transform')
        setTransformed(false);
    };
    const endListener = (event: TransitionEvent) => {
      if (event.propertyName === 'transform')
        setTransformed(true);
    };
    panel.addEventListener('transitionstart', startListener);
    panel.addEventListener('transitionend', endListener);
    return () => {
      panel.removeEventListener('transitionstart', startListener);
      panel.removeEventListener('transitionend', endListener);
    };
  }, [panelRef]);
  useEffect(() => {
    if (transformed && !opened)
      scrollToActiveChapter();
  }, [transformed]);
  useEffect(() => scrollToActiveChapter(), [chapter]);
  useEffect(() => {
    if (opened && transformed) {
      if (hoverable) return;
      const listener = () => {
        setHoverable(true);
        document.removeEventListener('mousemove', listener);
      };
      document.addEventListener('mousemove', listener);
      return () => document.removeEventListener('mousemove', listener);
    } else setHoverable(false);
  }, [opened, transformed, hoverable]);
  useEffect(() => {
    if (!sidePanelOpenedByMouse) return;
    const listener = (e: MouseEvent) => {
      if (e.x > panelRef.current!.clientWidth + 200)
        onClose();
    };
    document.addEventListener('mousemove', listener);
    return () => document.removeEventListener('mousemove', listener);
  }, [sidePanelOpenedByMouse]);
  useEffect(() => {
    if (coverpageRef.current)
      setCoverpageWidth(coverpageRef.current.width);
  }, [book]);
  return <Overlay opened={opened} onOverlayClick={onClose}>
    <div className={cn(styles.container, opened && styles.opened)} ref={panelRef}>
      <div className={styles.header}>
        {coverpageContent && <div className={styles.coverpageContainer} style={{width: `${coverpageWidth}px`}}>
          <img src={coverpageContent} className={styles.coverpage} ref={coverpageRef}/>
        </div>}
        <div className={styles.info}>
          <p className={styles.cycleName} title={cycleSize.toString()}>{cycleName}</p>
          {books.map((b) => <p onClick={() => setBook(b)}>{b.title}</p>)}
        </div>
      </div>
      <div className={styles.titleContainer}>
        <p className={styles.title}>{book.title}</p>
        <p className={styles.size}>{book.size}</p>
      </div>
      <div className={styles.chaptersList}>
        {book.chapters.map((ch) => {
          const is_active = ch.number === chapter.number;
          const cls = cn(styles.chapter, is_active && styles.active, hoverable && styles.hoverable);
          return <div className={cls} onClick={() => setChapter(ch)}>
            <p title={ch.title}>{ch.title}</p>
            <p>{ch.size}</p>
          </div>;
        })}
      </div>
    </div>
  </Overlay>;
};
