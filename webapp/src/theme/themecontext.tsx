import React, {createContext, useEffect, useMemo, useState} from 'react';
import defaultTheme from './default.theme.module.scss';

interface iTheme {
  root: string;
}

interface iThemeContext {
  theme: iTheme;
  setTheme: (theme: iTheme) => void;
}

export const ThemeContext = createContext<iThemeContext>(defaultTheme);

export const ThemeContextProvider = ({children}: {children: React.ReactNode}) => {
  const [theme, setTheme] = useState<iTheme>(defaultTheme);
  const state = useMemo<iThemeContext>(() => ({theme, setTheme}), [theme]);
  useEffect(() => {
    document.body.classList.toggle(theme.root, true);
    return () => {
      document.body.classList.toggle(theme.root, false)
    };
  }, [theme]);
  return <ThemeContext.Provider value={state}>
    {children}
  </ThemeContext.Provider>
}
