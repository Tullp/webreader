import React, {useEffect, useRef, useState} from 'react';
import styles from './fullscreen-image.module.scss';

export const useFullscreenImage = () => {
    const [expandedImage, setExpandedImage] = useState<HTMLImageElement>();
    const imageRef = useRef<HTMLImageElement>(null);
    useEffect(() => {
      if (!imageRef.current)
        return;

      const image = imageRef.current!;

      const handleImageLoad = () => {
        const container = image.parentElement!;
        const containerWidth = container.offsetWidth;
        const containerHeight = container.offsetHeight;
        const imageRatio = image.naturalWidth / image.naturalHeight;
        const containerRatio = containerWidth / containerHeight;

        if (imageRatio > containerRatio) {
          image.style.width = '100%';
          image.style.height = 'auto';
        } else {
          image.style.width = 'auto';
          image.style.height = '100%';
        }
        image.style.display = '';
      };

      image.addEventListener('load', handleImageLoad);
      if (image.complete)
        handleImageLoad();

      return () => image.removeEventListener('load', handleImageLoad);
    }, [imageRef.current]);
    useEffect(() => {
      if (!expandedImage && imageRef.current)
        imageRef.current.style.display = 'none';
    }, [expandedImage]);
    const listener = () => setExpandedImage(undefined);
    const display = expandedImage ? 'flex' : 'none';
    const FullscreenImage = <div className={styles.imageLayer} onClick={listener} style={{display}}>
      <div className={styles.imageContainer}>
        <img src={expandedImage && expandedImage.src} ref={imageRef}/>
      </div>
    </div>;
    return {FullscreenImage, setExpandedImage};
};
