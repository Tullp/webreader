import React, {forwardRef, useImperativeHandle, useMemo, useRef} from 'react';
import styles from './input.module.scss';
import {cn, keyBinding} from '../utils';
import {SearchOutline as SearchIcon} from 'react-ionicons';

interface iInput {
  value: string | undefined;
  placeholder?: string;
  width?: number;
  onKeyDown?: (event: React.KeyboardEvent<HTMLInputElement>) => void;
  onChange?: (value: string) => void;
  onFocus?: () => void;
  onBlur?: () => void;
  onApply?: (value: string) => void;
  custom_class?: string;
  searchIconSize?: number;
}

export interface iInputRef {
  focus: () => void;
  blur: () => void;
  value: () => string;
}

export const Input = forwardRef((props: iInput, ref: React.ForwardedRef<iInputRef>) => {
  const {value, placeholder, width, onChange, onKeyDown,
    onFocus, onBlur, onApply, custom_class, searchIconSize} = props;
  const inputRef = useRef<HTMLInputElement>(null);
  useImperativeHandle<iInputRef, iInputRef>(ref, () => ({
    focus: () => inputRef.current!.focus(),
    blur: () => inputRef.current!.blur(),
    value: () => inputRef.current!.value,
  }), []);
  const innerOnKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (keyBinding('Escape', event.nativeEvent)) {
      event.preventDefault();
      event.stopPropagation();
      inputRef.current!.blur();
    }
    else if (keyBinding('Enter', event.nativeEvent))
      onApply && onApply(inputRef.current!.value);
    onKeyDown && onKeyDown(event);
  };
  const cls = cn(styles.input, searchIconSize && styles.withSearchIcon, custom_class);
  const style = useMemo(() => Object.assign(
    {'--icon-size': `${searchIconSize}px`},
    width ? {width: `${width}px`} : {}
  ), [searchIconSize, width]);
  return <div className={styles.container}>
    {searchIconSize && <div className={styles.searchIcon} style={{'--size': `${searchIconSize}px`} as React.CSSProperties}>
      <SearchIcon color="#BDB7AF" width={searchIconSize} height={searchIconSize}/>
    </div>}
    <input ref={inputRef} type="text" className={cls} placeholder={placeholder} value={value}
      style={style} onKeyDown={innerOnKeyDown} onFocus={onFocus} onBlur={onBlur}
      onChange={(e) => onChange && onChange(e.target.value)}/>
  </div>;
});
