import React, {createContext, useContext, useMemo, useState} from 'react';
import {AppContext} from '../appcontext';

export const settingsConfig = [
  {
    title: 'Case sensitive search',
    controlId: 'caseSensitivity',
    checked: true,
  },
  {
    title: 'Word boundaries search',
    controlId: 'wordBoundary',
    checked: false,
  },
  {
    title: 'Right emphasises',
    controlId: 'emphasisRight',
    checked: false,
  },
  {
    title: 'Expand first book',
    controlId: 'expandFirstBook',
    checked: false,
  },
  {
    title: 'Side panel wide opening range',
    controlId: 'sidePanelWideOpeningRange',
    checked: false,
  },
  {
    title: 'Remember images tab',
    controlId: 'rememberImagesTab',
    checked: false,
  },
  {
    title: 'Open fullscreen images',
    controlId: 'openFullscreenImages',
    checked: false,
  },
] as const;

export type iSettings = { [id in typeof settingsConfig[number]['controlId']]: boolean };

interface iSettingsContext {
  settings: iSettings;
  changeSetting: (id: typeof settingsConfig[number]['controlId'], checked: boolean) => void;
}

export const SettingsContext = createContext<iSettingsContext>({} as iSettingsContext);

/**
 * A context provider component for managing application settings.
 *
 * @param {Object} props - The component props.
 * @param {React.ReactNode} props.children - The child components.
 * @returns The rendered wrapper.
 */
export const SettingsContextProvider = ({children}: {children: React.ReactNode}) => {
  const {cycleName} = useContext(AppContext);
  const key = (id: string) => `settings-${cycleName}-${id}`;
  const getKey = (id: string, def?: boolean) => {
    return JSON.parse(localStorage.getItem(key(id)) || def?.toString() || 'false');
  };
  const setKey = (id: string, value: boolean) => localStorage.setItem(key(id), value.toString());
  const [settings, setSettings] = useState<iSettings>(() => {
    return settingsConfig.reduce((a, item) => {
      return Object.assign(a, {[item.controlId]: getKey(item.controlId, item.checked)})
    }, {}) as iSettings;
  });
  const changeSetting = (id: string, checked: boolean) => {
    setSettings((p) => ({...p, [id]: checked}));
    setKey(id, checked);
  };
  const state = useMemo<iSettingsContext>(() => {
    return {settings, changeSetting};
  }, [settings]);
  return <SettingsContext.Provider value={state}>
    {children}
  </SettingsContext.Provider>;
};
