import React, {useContext, useState} from 'react';
import styles from './settings.module.scss';
import {Settings as SettingsIcon} from 'react-ionicons';
import {settingsConfig, SettingsContext} from './settingscontext';
import {cn} from '../utils';
import {AppContext} from '../appcontext';
import {Subwindow} from '../subwindow/subwindow';
import {Switch} from '../switch/switch';

/**
 * Represents a component for rendering settings button and modal.
 * @returns The rendered settings component.
 */
export const Settings = () => {
  const {setHeaderBlocker} = useContext(AppContext);
  const {settings, changeSetting} = useContext(SettingsContext);
  const [opened, setOpened] = useState<boolean>(false);
  const onOpen = (e: React.MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();
    setOpened(true);
    setHeaderBlocker('settings', true);
  };
  const onClose = () => {
    setOpened(false);
    setHeaderBlocker('settings', false);
  }
  return <div className={styles.container}>
    <SettingsIcon color="#BDB7AF" height="24px" width="24px" onClick={onOpen}/>
    <Subwindow opened={opened} side="left" onClose={onClose}>
      <div className={cn(styles.settings)}>
        {settingsConfig.map((item) => {
          return <div className={styles.row}>
            <p>{item.title}</p>
            <Switch onChange={(e) => changeSetting(item.controlId, e.target.checked)}
              checked={settings[item.controlId]}/>
          </div>;
        })}
      </div>
    </Subwindow>
  </div>;
};
