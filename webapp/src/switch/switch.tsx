
import React from 'react';
import styles from './switch.module.scss';

interface iSwitch {
  checked: boolean;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export const Switch = ({checked, onChange}: iSwitch) => {
    return <label className={styles.switch}>
      <input type="checkbox" checked={checked} onChange={onChange}/>
      <span className={styles.slider}/>
    </label>;
};
