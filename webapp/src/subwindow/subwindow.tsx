import React, {forwardRef, useEffect, useImperativeHandle, useMemo, useRef} from 'react';
import styles from './subwindow.module.scss';
import {cn, generateId} from '../utils';
import resizeIcon from './corner.png';

interface iSubwindow {
  children: React.ReactNode;
  opened: boolean;
  side: 'left' | 'right' | 'center';
  width?: number;
  height?: number;
  maxHeight?: number;
  onClose: () => void;
  noCloseSelectors?: string[];
  resize?: 'both' | 'vertical' | 'horizontal';
  dragElementId?: string;
}

export interface iScrollRef {
  scrollTo: (x: number, y: number) => void;
  scrollTop: () => number;
}

const parseTranslate3d = (translate: string) => {
  return translate.match(/translate3d\((?<x>-?\d+)px, (?<y>-?\d+)px, 0(?:px)?\)/)?.groups;
};

export const Subwindow = forwardRef((props: iSubwindow, ref: React.ForwardedRef<iScrollRef>) => {
  const {children, opened, side, width, height, maxHeight, onClose, noCloseSelectors, resize, dragElementId} = props;
  const windowRef = useRef<HTMLDivElement>(null);
  const scrollRef = useRef<HTMLDivElement>(null);
  useImperativeHandle<iScrollRef, iScrollRef>(ref, () => ({
    scrollTo: (x: number, y: number) => {
      console.log(`scrollTo inside imperative (${!!scrollRef.current})`);
      scrollRef.current!.scrollTo(x, y);
    },
    scrollTop: () => scrollRef.current!.scrollTop,
  }), [scrollRef]);
  const frameID = useRef<number>(0);
  const id = useMemo(() => generateId(), []);
  const selectors = useMemo(() => {
    if (!noCloseSelectors || noCloseSelectors.length === 0)
      return `#${id}`;
    return `#${id}, ` + noCloseSelectors.join(', ');
  }, [id, noCloseSelectors]);
  const setInitialSize = () => {
    const window = windowRef.current!;
    if (width !== undefined)
      window.style.width = `${width}px`;
    else window.style.removeProperty('width');
    if (height !== undefined)
      window.style.height = `${height}px`;
    else window.style.removeProperty('height');
    if (maxHeight !== undefined)
      window.style.maxHeight = `${maxHeight}px`;
    else window.style.removeProperty('maxHeight');
  };
  const onResizingStart = (event: React.MouseEvent) => {
    event.preventDefault();
    event.stopPropagation();
    document.body.style.userSelect = 'none';
    const {right, bottom} = windowRef.current!.getBoundingClientRect();
    const startMousePosition = {x: right - event.nativeEvent.x, y: bottom - event.nativeEvent.y};
    const doResize = (event: MouseEvent) => {
      const window = windowRef.current!;
      const {top, left} = window.getBoundingClientRect();
      if (resize === 'both' || resize === 'horizontal')
        window.style.width = `${event.x + startMousePosition.x - left}px`;
      if (resize === 'both' || resize === 'vertical')
        window.style.height = `${event.y + startMousePosition.y - top}px`;
    };
    const onResizingStop = () => {
      document.removeEventListener('mousemove', doResize);
      document.removeEventListener('mouseup', onResizingStop);
      document.body.style.userSelect = 'auto';
    };
    document.addEventListener('mousemove', doResize);
    document.addEventListener('mouseup', onResizingStop);
  };
  const onDragStart = (event: React.MouseEvent) => {
    const targetId = (event.target as HTMLElement).id;
    if (!dragElementId || targetId !== dragElementId) return;
    document.body.style.userSelect = 'none';
    const startMousePosition = {x: event.nativeEvent.x, y: event.nativeEvent.y};
    const match = parseTranslate3d(windowRef.current!.style.transform);
    const startWindowPosition = {x: parseInt(match?.x || '0'), y: parseInt(match?.y || '0')};
    const doDrag = (event: MouseEvent) => {
      const offsetX = event.x - startMousePosition.x + startWindowPosition.x;
      const offsetY = event.y - startMousePosition.y + startWindowPosition.y;
      cancelAnimationFrame(frameID.current);
      frameID.current = requestAnimationFrame(() => {
        windowRef.current!.style.transform = `translate3d(${offsetX}px, ${offsetY}px, 0)`;
      });
    };
    const onDragStop = () => {
      document.removeEventListener('mousemove', doDrag);
      document.removeEventListener('mouseup', onDragStop);
      document.body.style.userSelect = 'auto';
    };
    document.addEventListener('mousemove', doDrag);
    document.addEventListener('mouseup', onDragStop);
  }
  // Set initial size
  useEffect(() => {
    setInitialSize();
  }, []);
  // Close on click outside the window
  useEffect(() => {
    if (!opened) return;
    const listener = (e: MouseEvent) => {
      if (!e.target) return;
      const element = e.target as HTMLElement;
      if (!element.closest(selectors))
        onClose();
    };
    document.addEventListener('click', listener);
    return () => document.removeEventListener('click', listener);
  }, [opened]);
  // Restore initial size and position on close after resize and drag
  useEffect(() => {
    if (resize && !opened) {
      setInitialSize();
      windowRef.current!.style.removeProperty('transform');
    }
  }, [opened]);
  const cls = cn(styles.window, opened && styles.opened, styles[side]);
  return <div ref={windowRef} id={id} className={cls} onMouseDown={onDragStart}>
    <div className={styles.scrollContainer} ref={scrollRef}>{children}</div>
    {resize && <div className={styles.resizer} onMouseDown={onResizingStart}>
        <img src={resizeIcon}/>
    </div>}
  </div>;
});
