import React, {createContext, useEffect, useMemo, useRef, useState} from 'react';

interface iAppContext {
  cycleName: string;
  books: iBook[];
  book: iBook;
  chapter: iChapter;
  images: iBookImages[];
  setBook: (book: iBook) => void;
  nextBook: () => void;
  previousBook: () => void;
  setChapter: (chapter: iChapter) => void;
  nextChapter: () => void;
  previousChapter: () => void;
  next: () => void;
  previous: () => void;
  headerBlockers: {[p: string]: boolean};
  setHeaderBlocker: (id: string, value: boolean) => void;
}

export interface iBook {
  title: string;
  size: number;
  number: number;
  chapters: iChapter[];
}

export interface iChapter {
  bookTitle: string;
  title: string;
  size: number;
  content: HTMLElement;
  number: number;
}

export interface iBookImages {
  bookTitle: string;
  number: number;
  images: iImage[];
}

export interface iImage {
  bookTitle: string;
  content: HTMLImageElement,
  number: number;
}

/**
 * Retrieves all the books from the document and their corresponding chapters.
 *
 * @returns An array of book objects containing information on the books and their chapters.
 */
export const getBooks = () => {
  const extractTitle = (section: HTMLElement) => {
    const header = section.querySelector('header')?.cloneNode(true) as HTMLElement;
    if (!header) return 'Приложение';
    header.querySelector('.noteContainer')?.remove();
    return header.innerText.trim();
  }
  return [...document.querySelectorAll<HTMLElement>('#cycledata > div.book')!]
    .map((book: HTMLElement, index: number) => ({
      title: book.title,
      size: book.innerText.replaceAll('\n', '').length,
      number: index,
      chapters: [...book.querySelectorAll<HTMLElement>('& > section')]
        .map((section: HTMLElement, index: number) => ({
          bookTitle: book.title,
          content: section,
          size: section.innerText.replaceAll('\n', '').length,
          title: extractTitle(section),
          number: index,
        })),
    })) as iBook[];
};

export const getImages = () => {
  return [...document.querySelectorAll<HTMLElement>('#cycleimages > div.bookImages')!]
    .map((bookImages: HTMLElement, index: number) => ({
      bookTitle: bookImages.title,
      number: index,
      images: [...bookImages.querySelectorAll<HTMLElement>('& > img')]
        .map((img: HTMLElement, index: number) => ({
          bookTitle: bookImages.title,
          content: img.cloneNode(true),
          number: index,
        })),
    })) as iBookImages[];
}

export const AppContext = createContext<iAppContext>({} as iAppContext);

/**
 * Provides the application context for the React application.
 *
 * @param {Object} props - The component props.
 * @param {React.ReactNode} props.children - The child components.
 * @returns The rendered wrapper.
 */
export const AppContextProvider = ({children}: {children: React.ReactNode}) => {
  const books = useMemo(() => getBooks(), []);
  const images = useMemo(() => getImages(), []);
  const cycleName = decodeURIComponent(window.location.pathname.split('/').pop()!.slice(0, -5));
  const hashPointer = JSON.parse(decodeURIComponent(window.location.hash).slice(1) || 'null');
  if (hashPointer)
    window.location.hash = '';
  const getInt = (key: string) => parseInt(localStorage.getItem(key) || '') || 0;
  const cycleKey = `cycle-${cycleName}`;
  const bookKey = `${cycleKey}-book`;
  const bookNumber = hashPointer ? hashPointer.book : getInt(bookKey);
  const [book, setBook] = useState<iBook>(books[bookNumber]);
  const chapterKey = useMemo(() => `${cycleKey}-${book.title}`, [book]);
  const chapterNumber = hashPointer ? hashPointer.chapter : getInt(chapterKey);
  const [chapter, setChapter] = useState<iChapter>(book.chapters[chapterNumber]);
  const scrollKey = useMemo(() => `${cycleKey}-${book.title}-${chapter.number}-scroll`, [book, chapter]);
  const scrollInited = useRef<boolean>(false);
  const [headerBlockers, setHeaderBlockers] = useState<{[p: string]: boolean}>({scroll: true});
  const setHeaderBlocker = (id: string, value: boolean) => {
    setHeaderBlockers((p) => ({...p, [id]: value}));
  };
  const state = useMemo<iAppContext>(() => {
    const nextBook = () => {
      if (book.number === books.length - 1)
        return;
      setBook(books[book.number + 1]);
    };
    const previousBook = () => {
      if (book.number === 0)
        return;
      setBook(books[book.number - 1]);
    };
    const nextChapter = () => {
      if (chapter.number === book.chapters.length - 1)
        return;
      setChapter(book.chapters[chapter.number + 1]);
    };
    const previousChapter = () => {
      if (chapter.number === 0)
        return;
      setChapter(book.chapters[chapter.number - 1]);
    };
    const next = () => {
      if (chapter.number < book.chapters.length - 1)
        nextChapter();
      else nextBook();
    }
    const previous = () => {
      if (chapter.number > 0)
        previousChapter();
      else previousBook();
    }
    return {cycleName, books, book, chapter, images, setChapter, setBook, nextBook, nextChapter,
      previousBook, previousChapter, next, previous, headerBlockers, setHeaderBlocker};
  }, [books, book, chapter, headerBlockers]);
  useEffect(() => {
    localStorage.setItem(bookKey, book.number + '');
    document.title = book.title;
    setChapter(book.chapters[chapterNumber]);
  }, [book]);
  useEffect(() => {
    localStorage.setItem(chapterKey, chapter.number + '');
    const position = getInt(scrollKey);
    if (scrollInited.current)
      window.scrollTo(0, position);
    else {
      setTimeout(() => window.scrollTo(0, position), 100);
      scrollInited.current = true
    }
    return () => localStorage.removeItem(scrollKey);
  }, [chapter]);
  useEffect(() => {
    const listener = () => {
      localStorage.setItem(scrollKey, window.scrollY + '');
      localStorage.setItem(bookKey, book.number + '');
      localStorage.setItem(chapterKey, chapter.number + '');
    };
    document.addEventListener('scroll', listener);
    return () => document.removeEventListener('scroll', listener);
  }, [scrollKey, book, chapter]);
  return <AppContext.Provider value={state}>
    {children}
  </AppContext.Provider>;
};
