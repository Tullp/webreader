
/**
 * Concatenates a variable number of strings into a single string, discarding any falsy values.
 *
 * @param {...any} strings - The strings to concatenate.
 * @returns The concatenated string.
 */
export const cn = (...strings: any[]) => {
  return strings.filter((s) => !!s).join(' ');
};

/**
 * Generates sequence of N 16-bit numbers
 *
 * @param {number} n - Length of the sequence.
 * @returns String with N 16-bit numbers.
 */
export const generateId = (n: number = 10) => {
  const random16Number = () => (Math.random() * 16 | 0).toString(16);
  return 'id' + Array(n).fill(null).map(random16Number).join('')
};

export const keyBinding = (binding: string, event: KeyboardEvent) => {
  for (let key of binding.split(/\s*\+\s*/)) {
    if (key === 'cmd') {
      if (!event.metaKey) return false;
    } else if (key === 'shift') {
      if (!event.shiftKey) return false;
    } else if (key.toLowerCase() !== event.key.toLowerCase())
      return false;
  }
  return true;
};
