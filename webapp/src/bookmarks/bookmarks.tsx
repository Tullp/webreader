import React, {useCallback, useContext, useEffect, useMemo, useState} from 'react';
import styles from './booksmarks.module.scss';
import {Bookmarks as BookmarksIcon} from 'react-ionicons';
import {AppContext} from '../appcontext';
import {Subwindow} from '../subwindow/subwindow';
import {Add as PlusIcon} from 'react-ionicons'
import {generateId, keyBinding} from '../utils';
import {Input} from '../input/input';

interface iBookmark {
  id: string;
  note: string;
  book: number;
  chapter: number;
  date: number;
}

class BookmarksStorage {

  key: string;
  constructor(cycleName: string) { this.key = `bookmarks-${cycleName}` }

  get() { return JSON.parse(localStorage.getItem(this.key) || '[]') as iBookmark[] }

  set(bookmarks: iBookmark[]) { localStorage.setItem(this.key, JSON.stringify(bookmarks)) }
}

const dateSort = (a: iBookmark, b: iBookmark) => a.date - b.date;

export const Bookmarks = () => {
  const {cycleName, book, chapter, setHeaderBlocker} = useContext(AppContext);
  const storage = useMemo(() => new BookmarksStorage(cycleName), [cycleName]);
  const [bookmarks, setBookmarks] = useState<iBookmark[]>(storage.get());
  const [filterQuery, setFilterQuery] = useState<string>();
  const [appliedFilterQuery, setAppliedFilterQuery] = useState<string>();
  const preparedBookmarks = useMemo(() => bookmarks.slice()
    .filter((bm) => bm.note.includes(appliedFilterQuery || ''))
    .sort(dateSort), [bookmarks, appliedFilterQuery]);

  const [opened, setOpened] = useState<boolean>(false);
  const onOpen = (e?: React.MouseEvent) => {
    e?.preventDefault();
    e?.stopPropagation();
    setOpened(true);
    setHeaderBlocker('bookmarks', true);
  };
  const onClose = () => {
    setOpened(false);
    setFilterQuery('');
    setAppliedFilterQuery('');
    setHeaderBlocker('bookmarks', false);
  }
  useEffect(() => {
    const listener = (e: KeyboardEvent) => {
      if (!opened && keyBinding('cmd+b', e)) {
        e.preventDefault();
        e.stopPropagation();
        onOpen();
        if (window.getSelection()?.toString().length) {
          const query = window.getSelection()!.toString();
          setFilterQuery(query)
          setAppliedFilterQuery(query);
          window.getSelection()!.empty();
        }
      } else if (opened && keyBinding('escape', e)) {
        e.preventDefault();
        e.stopPropagation();
        onClose();
      }
    }
    document.addEventListener('keydown', listener);
    return () => document.removeEventListener('keydown', listener);
  }, [opened]);

  const createBookmark = useCallback(() => {
    setBookmarks((bm) => [...bm, {
      id: generateId(), note: '',
      book: book.number, chapter: chapter.number,
      date: Date.now(),
    }]);
  }, [book, chapter]);
  const removeBookmark = (id: string) => {
    setBookmarks((bm) => bm.slice().filter((bm) => bm.id !== id));
  };
  const updateBookmark = (id: string, note: string) => {
    setBookmarks((bookmarks) => {
      const bookmark = bookmarks.find((bm) => bm.id === id)!;
      bookmark.note = note;
      return bookmarks.slice();
    });
  }
  const onKeyDown = (id: string, event: React.KeyboardEvent<HTMLParagraphElement>) => {
    if (event.key === 'Escape') {
      event.preventDefault();
      event.stopPropagation();
      const target = (event.target as HTMLParagraphElement);
      target.blur();
      if (target.innerText.trim().length === 0)
        removeBookmark(id);
    }
  };
  const onBlur = (id: string, event: React.FocusEvent<HTMLParagraphElement>) => {
    updateBookmark(id, event.target.innerText);
  };

  useEffect(() => {
    storage.set(bookmarks);
  }, [storage, bookmarks]);

  return <div className={styles.container}>
    <div className={styles.openBookmarks} onClick={onOpen}>
      <BookmarksIcon color="#BDB7AF" width={22} height={22}/>
    </div>
    <Subwindow opened={opened} side="right" width={425} maxHeight={600}
        onClose={onClose} resize="both" dragElementId="draggable">
      <div className={styles.window} id="draggable">
        <div className={styles.header}>
          <div className={styles.inputContainer}>
            <Input custom_class={styles.input} value={filterQuery} placeholder="Поиск" searchIconSize={20}
              onChange={setFilterQuery} onApply={setAppliedFilterQuery}/>
          </div>
          <div className={styles.plus} onClick={createBookmark}>
            <PlusIcon color="#BDB7AF" width={24} height={24}/>
          </div>
        </div>
        <div className={styles.bookmarks}>
          {preparedBookmarks.map((bookmark) => <div className={styles.bookmark}>
            <p contentEditable="plaintext-only"
                suppressContentEditableWarning={true}
                spellCheck={false}
                onKeyDown={(e) => onKeyDown(bookmark.id, e)}
                onBlur={(e) => onBlur(bookmark.id, e)}>
              {bookmark.note}
            </p>
          </div>)}
        </div>
      </div>
    </Subwindow>
  </div>;
};
