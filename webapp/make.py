
import os

def normalize(filename):
	return filename.rsplit(".", 2)[0] + "." + filename.rsplit(".", 1)[-1]

js_file = [file for file in os.listdir("build/static/js") if file.endswith(".js")][0]
css_file = [file for file in os.listdir("build/static/css") if file.endswith(".css")][0]
js_normalized = normalize(js_file)
css_normalized = normalize(css_file)

os.rename(f"build/static/js/{js_file}", f"build/static/js/{js_normalized}")
os.rename(f"build/static/css/{css_file}", f"build/static/css/{css_normalized}")

with open("build/index.html", "r", encoding="utf-8") as f:
	html = f.read()
html = html.replace(f"build/static/js/{js_file}", f"build/static/js/{js_normalized}")
html = html.replace(f"build/static/css/{css_file}", f"build/static/css/{css_normalized}")
with open("build/index.html", "w", encoding="utf-8") as f:
	f.write(html)
