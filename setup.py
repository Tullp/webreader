
import os
import sys
import json
import platform

root = os.path.dirname(os.path.realpath(__file__))
os.chdir(root)

if not os.path.exists("pages"):
	os.mkdir("pages")

def ask(name: str, desc: str):
	default = f" (default: {old_config.get(name)})" if name in old_config else ""
	config.update({name: input(f"{desc}{default}: ") or old_config.get(name)})

old_config = {}
if os.path.exists("config.json"):
	try:
		with open("config.json", "r") as f:
			old_config = json.load(f)
	except Exception:
		pass

config = {}
ask("booksStorage", "Enter full path of books storage directory")
ask("botToken", "Enter telegram bot token for exceptions broadcasting")
ask("telegramId", "Enter telegram user id for exceptions broadcasting")
with open("config.json", "w") as f:
	json.dump(config, f)

if platform.system() == "Windows":
	python = os.path.join(root, "venv", "Scripts", "python.exe")
else:
	python = os.path.join(root, "venv", "bin", "python")
if not os.path.exists("venv"):
	os.system(f"{sys.executable} -m venv venv")
os.system(f"{python} -m pip install -r requirements.txt")

os.chdir(os.path.join(root, "webapp"))
if not os.path.exists("node_modules"):
	os.system("npm install")
if not os.path.exists("build"):
	os.system("npm run build")
os.chdir(root)

if platform.system() == "Darwin":
	app_path = f"{os.path.expanduser('~')}/Applications/WebReader.app"
	with open(os.path.join(root, "macos.script.template"), "r", encoding="utf-8") as template:
		with open(os.path.join(root, "macos.script"), "w", encoding="utf-8") as script:
			script.write(template.read().format(root=root))
	os.system(f"osacompile -o {app_path} {os.path.join(root, "macos.script")}")

if platform.system() == "Windows":
	with open(os.path.join(root, "windows.script.template"), "r") as template:
		with open(os.path.join(root, "windows.script"), "w") as script:
			script.write(template.read().format(root=root))
